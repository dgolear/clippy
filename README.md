# Clippy

- [Команды](docs/commands.md)
- [Схема работы](docs/diagram.png)
- [Конфигурация](docs/config.md)

---

[The Tragic Life of Clippy, the World's Most Hated Virtual Assistant](https://www.mentalfloss.com/article/504767/tragic-life-clippy-worlds-most-hated-virtual-assistant)